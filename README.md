Projeyi çalıştırabilmek için aşağıdaki adımları uygulamanız gerekmektedir.

* composer install
* ./vendor/bin/sail up
* sail artisan migrate
* sail artisan db:seed
* sail artisan queue:work

Yukarıdaki kodları çalıştırdığınız taktirde docker üzerinden PostgreSql veritabanı üzerinden çalışamaya başlayacaktır.

Proje ana dizini içerisinde bulunan Postman Collection üzerinden de apileri test edebilirsiniz.

Teşekkürler
