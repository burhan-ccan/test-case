<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $authors = [
            ['name' => 'Yazar 1'],
            ['name' => 'Yazar 2'],
            ['name' => 'Yazar 3'],
        ];

        foreach ($authors as $author) {
            Author::create($author);
        }

        // Kitapları oluştur
        $books = [
            ['title' => 'Kitap 1', 'author_id' => 1],
            ['title' => 'Kitap 2', 'author_id' => 1],
            ['title' => 'Kitap 3', 'author_id' => 2],
            ['title' => 'Kitap 4', 'author_id' => 3],
        ];

        foreach ($books as $book) {
            Book::create($book);
        }
    }
}
