<?php

namespace App\Jobs;

use App\Models\Book;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteBookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bookId;

    public function __construct($bookId)
    {
        $this->bookId = $bookId;
    }

    public function handle()
    {
        $book = Book::query()
            ->find($this->bookId);
        $bookCount = $book->count();
        if($bookCount > 0){
            $book->delete();
            return [ 'message' => 'Kayıt Silindi', 'status' => 'success' ];
        }
        else {
            return [ 'message' => 'Kayıt Bulunamadı', 'status' => 'error' ];
        }
    }
}

