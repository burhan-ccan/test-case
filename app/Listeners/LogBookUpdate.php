<?php

namespace App\Listeners;

use App\Models\Log;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogBookUpdate implements ShouldQueue
{
    public function handle($event)
    {
        $log = new Log();
        $log->message = $event->message->id . 'idli Kitap ' . date('Y-m-d H:i:s') . ' tarihinde güncellenmiştir.' ;
        $log->save();
    }
}
