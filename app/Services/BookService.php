<?php

namespace App\Services;

use App\Events\BookUpdated;
use App\Jobs\DeleteBookJob;
use App\Models\Book;
use App\Services\Shared\DataSourceService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;

class BookService
{
    private DataSourceService $dataSourceService;

    public function __construct(
        DataSourceService $dataSourceService
    )
    {
        $this->dataSourceService = $dataSourceService;
    }
    public function bookList()
    {
        $books = Cache::remember('bookList', 60, function () {
            return Book::with('author')->get();
        });

        return response()->json($books);
    }
    public function bookDelete($request)
    {
        $job = new DeleteBookJob($request->input('id'));
        Queue::push($job);

        return [
            'status' => 'success',
            'message' => 'Kayıt Silme İşlemi Alınmıştır.',
        ];
    }

    public function bookEdit($request)
    {
        $formData = array_intersect_key($request->all(), Arr::except($request->rules(), ['key']));
        $entity = Book::query()
            ->find($request->input('key'));

        $entity->update($formData);

        event(new BookUpdated($entity));

        $status = true;
        $message = 'Kitap güncellendi.';
        $data = $entity;

        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
    }
}
