<?php

namespace App\Services\Shared;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DataSourceService
{
    public function __construct()
    {
    }

    public function getData($query, Request $request)
    {

        $response = array();

        $requireTotalCount = $request->get('requireTotalCount');
        $skip = $request->get('skip');
        $take = $request->get('take');
        $sort = $request->get('sort');
        $filter = $request->get('filter');
        $group = $request->get('group');

        if (isset($sort) && is_array(json_decode($sort)) && !isset($group)) {
            $sortArray = json_decode($sort);
            foreach ($sortArray as $key => $sortItem) {
                $query->orderBy($sortItem->selector, $sortItem->desc ? 'asc' : 'desc');
            }
        }

        if (isset($filter) && is_array(json_decode($filter))) {
            $query->where(function ($q) use ($filter) {
                $q->wheres = [];

                $isCriteria = function ($item) {
                    return is_array($item) && !is_string($item);
                };

                $filterArray = json_decode($filter);

                $compileWhereOperator = function ($whereOp, $whereValue) {
                    switch ($whereOp) {
                        case 'contains':
                            $whereOp = 'ILIKE';
                            $whereValue = '%' . $whereValue . '%';
                            break;
                        case 'notcontains':
                            $whereOp = 'NOT ILIKE';
                            $whereValue = '%' . $whereValue . '%';
                            break;
                        case 'startswith':
                            $whereOp = 'ILIKE';
                            $whereValue = $whereValue . '%';
                            break;
                        case 'endswith':
                            $whereOp = 'ILIKE';
                            $whereValue = '%' . $whereValue;
                            break;
                    }

                    return ['operator' => $whereOp, 'value' => $whereValue];
                };

                if (!$isCriteria($filterArray[0])) {
                    $compiledWhere = $compileWhereOperator($filterArray[1], $filterArray[2]);

                    $whereOperator = $compiledWhere['operator'];
                    $filterValue = $compiledWhere['value'];

                    if ($filterArray[1] == 'BETWEEN') {
                        $q->whereBetween($filterArray[0], $filterArray[2]);
                    } else {
                        $q->where($filterArray[0], $whereOperator, $filterValue);
                    }
                } else {
                    $compileFilter = function ($filterCriteria, $query) use ($compileWhereOperator, &$compileFilter) {

                        $operand = in_array('and', $filterCriteria) ? 'and' : 'or';

                        foreach ($filterCriteria as $filterKey => $filterValue) {
                            if (is_array($filterValue) && is_string($filterValue[0])) {
                                if (count($filterValue) === 2) {
                                    $query = $query->where($filterValue[0], $filterValue[1]);
                                } elseif (count($filterValue) === 3) {
                                    $compiledWhere = $compileWhereOperator($filterValue[1], $filterValue[2]);
                                    $whereOperator = $compiledWhere['operator'];
                                    $whereValue = $compiledWhere['value'];
                                    $query = $query->where($filterValue[0], $whereOperator, $whereValue, $operand);
                                }
                            } elseif (is_array($filterValue) && !is_string($filterValue[0])) {
                                $t = $compileFilter($filterValue, $query->forNestedWhere());
                                $query->addNestedWhereQuery($t, $operand);
                            } elseif (is_string($filterValue)) {
                                $operand = $filterValue;
                            }
                        }

                        return $query;
                    };

                    $q = $compileFilter($filterArray, $q);
                }
            });
        }

        if (isset($requireTotalCount) || isset($skip)) {
            $response['totalCount'] = $query->count();
        }

        if (isset($skip)) {
            $query->skip($skip);
        }

        if (isset($take)) {
            $query->take($take);
        }

        $response['data'] = $query->get();

        return $response;
    }
}
