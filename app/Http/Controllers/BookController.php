<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteBook;
use App\Http\Requests\UpdateBook;
use App\Services\BookService;

class BookController extends Controller
{
    private BookService $bookService;

    public function __construct(
        BookService $bookService
    ) {
        $this->bookService = $bookService;
    }
    public function index()
    {
        return view('index');
    }
    public function bookList(): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->bookService->bookList());
    }
    public function bookDelete(DeleteBook $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->bookService->bookDelete($request));
    }
    public function bookEdit(UpdateBook $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->bookService->bookEdit($request));
    }


}
