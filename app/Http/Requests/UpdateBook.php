<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required|exists:books,id',
            'title' => 'string|max:200',
            'author_id' => 'required|exists:authors,id',
        ];
    }
    public function attributes(): array
    {
        return [
            'title' => 'Book Name',
            'author_id' => 'Author'
        ];
    }
}
